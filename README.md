# dark_souls_backup

This project contains a series of Bash shellscripts that can be scheduled to automatically back up your saved game files from the Dark Souls video game series, including Dark Souls Remastered, Dark Souls 2: Scholar of the First Sin, and Dark Souls 3.

Note: There are a few caveats to remember when using this project:

1) This project assumes that you are a GNU/Linux user, using the Debian (or a derivative-of-Debian) distribution.
2) This project assumes you are using Steam for GNU/Linux, which can be obtained through your distribution's package
   manager, or by visiting the following link:

	https://store.steampowered.com/about/
3) This project assumes that, since you are a Steam GNU/Linux user, you are using Valve's SteamPlay/Proton capabilities:
	- This means you are NOT using vanilla Wine (with or without DXVK), only Steam's built-in SteamPlay/Proton feature.
	- For more information on SteamPlay and/or Proton, visit the following links:

		https://steamcommunity.com/games/221410/announcements/detail/1696055855739350561

		https://github.com/ValveSoftware/Proton/
