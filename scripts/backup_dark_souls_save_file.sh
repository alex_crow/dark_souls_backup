#!/bin/bash

DARK_SOULS_PREFIX=${1}
DARK_SOULS_SAVE_PATH=${2}
DARK_SOULS_ENTRY=${3}

if [ ${DARK_SOULS_ENTRY} == "1" ]; then
  DARK_SOULS_ENTRY="remastered"
elif [ ${DARK_SOULS_ENTRY} == "2S" ]; then
  DARK_SOULS_ENTRY="2_-_scholar_of_the_first_sin"
fi

SOURCE_DIR="${DARK_SOULS_PREFIX}${DARK_SOULS_SAVE_PATH}"
TARGET_DIR="../save_files/dark_souls_${DARK_SOULS_ENTRY}"

cp "${SOURCE_DIR}" "${TARGET_DIR}"
