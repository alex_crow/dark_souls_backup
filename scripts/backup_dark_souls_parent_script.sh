#!/bin/bash

STEAMPLAY_DIR="${HOME}/.steam/steam/steamapps/compatdata"

DARK_SOULS_1_PREFIX="${STEAMPLAY_DIR}/570940/pfx"
DARK_SOULS_2_PREFIX="${STEAMPLAY_DIR}/236430/pfx"
DARK_SOULS_2S_PREFIX="${STEAMPLAY_DIR}/335300/pfx"
DARK_SOULS_3_PREFIX="${STEAMPLAY_DIR}/374320/pfx"

PREFIX_ROOT="/drive_c/users/steamuser"

DARK_SOULS_1_SAVE_PATH="${PREFIX_ROOT}/My Documents/NBGI/DARK SOULS REMASTERED/11213410/DRAKS0005.sl2"
DARK_SOULS_2_SAVE_PATH="${PREFIX_ROOT}/Application Data/DarkSoulsII/0110000100ab1a62/DARKSII0000.sl2"
DARK_SOULS_2S_SAVE_PATH="${PREFIX_ROOT}/Application Data/DarkSoulsII/0110000100ab1a62/DS2SOFS0000.sl2"
DARK_SOULS_3_SAVE_PATH="${PREFIX_ROOT}/Application Data/DarkSoulsIII/0110000100ab1a62/DS30000.sl2"

function backup_game {
  case ${1} in
    "1")
      GAME_PREFIX="${DARK_SOULS_1_PREFIX}"
      GAME_SAVE_PATH="${DARK_SOULS_1_SAVE_PATH}"
      ;;
    "2")
      GAME_PREFIX="${DARK_SOULS_2_PREFIX}"
      GAME_SAVE_PATH="${DARK_SOULS_2_SAVE_PATH}"
      ;;
    "2S")
      GAME_PREFIX="${DARK_SOULS_2S_PREFIX}"
      GAME_SAVE_PATH="${DARK_SOULS_2S_SAVE_PATH}"
      ;;
    "3")
      GAME_PREFIX="${DARK_SOULS_3_PREFIX}"
      GAME_SAVE_PATH="${DARK_SOULS_3_SAVE_PATH}"
      ;;
  esac

  if [ -f "${GAME_PREFIX}/${GAME_SAVE_PATH}" ]; then
    echo
    echo "[INFO]: Attempting to back up Dark Souls ${1} save file..."
    ./backup_dark_souls_save_file.sh "${GAME_PREFIX}" "${GAME_SAVE_PATH}" "${1}"
    RETURN_CODE=${?}

    if [ ${RETURN_CODE} -eq 0 ]; then
      echo
      echo "[SUCCESS]: Dark Souls ${1} save file successfully backed up!"
    else
      echo
      echo "[ERROR]: Something went wrong when attempting to back up Dark Souls ${1} save file. Exiting..."
      exit ${RETURN_CODE}
    fi

  else
    echo
    echo "[INFO]: Dark Souls ${1} save file does not exist."
  fi
}

# Backing up Dark Souls Remastered save file:
backup_game "1"

# Backing up Dark Souls 2 save file:
backup_game "2"

# Backing up Dark Souls 2: Scholar of the First Sin save file:
backup_game "2S"

# Backing up Dark Souls 3 save file:
backup_game "3"

# Backing up all save-files remotely

git add .. && git commit -m "Committing latest Dark Souls save game files." && git push
